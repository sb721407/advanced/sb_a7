##### VMs Part #####

resource "yandex_compute_instance" "server" {
  name        = "server"
  platform_id = "standard-v2"
  resources {
    core_fraction = 50
    cores         = "4"
    memory        = "8"
  }
  boot_disk {
    initialize_params {
      #      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      image_id = "fd8151sv1q69mchl804a" # centos-8
      size     = 20
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "client" {
  name        = "client"
  platform_id = "standard-v2"
  resources {
    core_fraction = 20
    cores         = "2"
    memory        = "2"
  }
  boot_disk {
    initialize_params {
      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      #image_id = "fd87697emk1o3pa6iuva" # lemp
      #image_id = "fd8151sv1q69mchl804a" # centos-8
      size = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "centos" {
  name        = "centos"
  platform_id = "standard-v2"
  resources {
    core_fraction = 20
    cores         = "2"
    memory        = "2"
  }
  boot_disk {
    initialize_params {
      #image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      #image_id = "fd87697emk1o3pa6iuva" # lemp
      image_id = "fd8151sv1q69mchl804a" # centos-8
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}



##### Create file inventory #####

resource "local_file" "inventory" {
  content  = <<EOF

[server]
${yandex_compute_instance.server.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

[client]
${yandex_compute_instance.client.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

[centos]
${yandex_compute_instance.centos.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

[all]
${yandex_compute_instance.server.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa
${yandex_compute_instance.client.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa
${yandex_compute_instance.centos.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa

EOF
  filename = "${path.module}/inventory"
}

##### Provisioning with Ansible VM client #####

resource "null_resource" "client" {
  depends_on = [yandex_compute_instance.client, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.client.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --private-key ~/.ssh/id_rsa ../ansible/pb-client.yml"
  }
}

##### Provisioning with Ansible VM server #####

resource "null_resource" "server" {
  depends_on = [yandex_compute_instance.server, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.server.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --private-key ~/.ssh/id_rsa ../ansible/pb-server.yml"
  }
}

##### Provisioning with Ansible VM centos #####

resource "null_resource" "centos" {
  depends_on = [yandex_compute_instance.centos, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.centos.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --private-key ~/.ssh/id_rsa ../ansible/pb-centos.yml"
  }
}


##### Add A-record to DNS zone #####

resource "yandex_dns_recordset" "server_dns_name" {
  depends_on = [yandex_compute_instance.server, local_file.inventory]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "server"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.server.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "client_dns_name" {
  depends_on = [yandex_compute_instance.client, local_file.inventory]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "client"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.client.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "centos_dns_name" {
  depends_on = [yandex_compute_instance.centos, local_file.inventory]
  zone_id    = "dns6bnf8lr6972tjennl"
  name       = "centos"
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.centos.network_interface.0.nat_ip_address]
}

